<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller{
    /**
     * @Route("/", name="homepage")
     */
    public function home(){
        $tableau = ["Person 1" => 36, "Person 2" => 20, "Person 3" => 15];

        return $this->render("home.html.twig",[
            "title" => "Bonjour à tous",
            "age" => 31,
            "tableau" => $tableau
        ]);
    }
}